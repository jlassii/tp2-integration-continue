import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserComponent } from './user-component';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {HttpService} from '../../services/http-service';
import {of} from 'rxjs';

describe('UserComponentComponent', () => {
  let component: UserComponent;
  let fixture: ComponentFixture<UserComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UserComponent ],
      imports: [HttpClientTestingModule],
      providers: [
        {
          provide: HttpService,
          useValue: { getUsers: () => of([]) }
        }
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
