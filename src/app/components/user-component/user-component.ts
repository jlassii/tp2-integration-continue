import { Component, OnInit } from '@angular/core';
import {Users, HttpService} from '../../services/http-service';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-user-component',
  templateUrl: './user-component.html',
  styleUrls: ['./user-component.css']
})
export class UserComponent implements OnInit {
  public users$: Observable<Users[]> ;

  constructor( private userService: HttpService) { }

  ngOnInit(): void {
    this.users$ = this.userService.getUsers();
  }
}
