import { Component, OnInit } from '@angular/core';
import {Chatrooms, HttpService} from '../../services/http-service';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-chat-component',
  templateUrl: './chat-component.html',
  styleUrls: ['./chat-component.css']
})
export class ChatComponent implements OnInit {
  chats$: Observable<Chatrooms[]>;

  constructor(private chatService: HttpService) { }

  ngOnInit(): void {
    this.chats$ = this.chatService.getChatrooms();
  }

}
