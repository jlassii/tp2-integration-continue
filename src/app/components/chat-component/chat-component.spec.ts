import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChatComponent } from './chat-component';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {HttpService} from '../../services/http-service';
import {of} from 'rxjs';

describe('ChatComponentComponent', () => {
  let component: ChatComponent;
  let fixture: ComponentFixture<ChatComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChatComponent ],
      imports: [HttpClientTestingModule],
      providers: [
        {
          provide: HttpService,
          useValue: { getChatrooms: () => of([]) }
        }
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
