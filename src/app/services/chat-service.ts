import { Injectable } from '@angular/core';

export  interface Chatrooms{
  name: string;
}

@Injectable({
  providedIn: 'root'
})
export class ChatService {

  constructor() { }

  getChatrooms(): Chatrooms[]{
    return [
      {name: 'chat1'},
      {name: 'chat2'}
    ];
  }
}
