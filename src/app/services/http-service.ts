import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

export interface Users{
  userAccount: {
    userId: number,
    userName: string
  };
  currentstatus: string;
}


export  interface Chatrooms{
  chatName: string;
}

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  private usersUrl = 'http://localhost:2345/users';
  private chatsUrl = 'http://localhost:2345/chatrooms';

  constructor(private httpClient: HttpClient) {  }

  getUsers(): Observable<Users[]> {
    return this.httpClient.get<Users[]>(this.usersUrl);
  }

  getChatrooms(): Observable<Chatrooms[]> {
    return this.httpClient.get<Chatrooms[]>(this.chatsUrl);
  }
}
