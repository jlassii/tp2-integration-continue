import { Injectable } from '@angular/core';

export interface Users{
  userid: number;
  username: string;
  currentstatus: string;
}

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor() { }

  getUsers(): Users[]{
    return [
      {userid: 1, username: 'ousmane', currentstatus: 'actiuve'},
      {userid: 2, username: 'ramatoulaye', currentstatus: 'innactive'}
    ];
  }
}
