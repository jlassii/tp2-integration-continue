import { TestBed } from '@angular/core/testing';

import { HttpService } from './http-service';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {HttpClient} from '@angular/common/http';
import {of} from 'rxjs';

describe('UsersServiceService', () => {
  let service: HttpService;
  let httpGetSpy: jasmine.Spy;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule]
    });
    service = TestBed.inject(HttpService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should call http.get to get the users', () => {
    httpGetSpy = spyOn(TestBed.get(HttpClient), 'get').and.returnValues(of({}));
    const expectedRequestUrl = 'http://localhost:2345/users';

    service.getUsers();

    expect(httpGetSpy).toHaveBeenCalledWith(expectedRequestUrl);
  });
});
