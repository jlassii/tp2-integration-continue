import {Component, Input} from '@angular/core';
import {Observable} from 'rxjs';
import {Users} from './services/http-service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'TP2IntegrationContinue';
  @Input() users$: Observable<Users[]> ;

}
